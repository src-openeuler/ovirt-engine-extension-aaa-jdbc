%global make_common_opts \\\
	PREFIX=%{_prefix} \\\
	SYSCONF_DIR=%{_sysconfdir} \\\
	DATAROOT_DIR=%{_datadir} \\\
	DESTDIR=%{buildroot} \\\
	%{nil}


Name:		ovirt-engine-extension-aaa-jdbc
Version:	1.2.0
Release:	1
Summary:	oVirt Engine Local Users Management Extension
Group:		%{ovirt_product_group}
License:	ASL 2.0
URL:		http://www.ovirt.org
Source0:	%{name}-%{version}.tar.gz

# We need to disable automatic generation of "Requires: java-headless >= 1:11"
# by xmvn, becase JDK 11 doesn't provide java-headless artifact, but it
# provides java-11-headless.
AutoReq:	no

BuildArch:	noarch

BuildRequires:	java-11-openjdk-devel
BuildRequires:	maven-local
BuildRequires:	mvn(org.apache.maven.plugins:maven-compiler-plugin)
BuildRequires:	mvn(org.apache.maven.plugins:maven-source-plugin)

BuildRequires:	mvn(commons-codec:commons-codec)
BuildRequires:	mvn(com.fasterxml.jackson.core:jackson-core)
BuildRequires:	mvn(com.fasterxml.jackson.core:jackson-databind)
BuildRequires:	mvn(org.apache.commons:commons-lang)
BuildRequires:	mvn(org.ovirt.engine.api:ovirt-engine-extensions-api)
BuildRequires:	mvn(org.slf4j:slf4j-api)
BuildRequires:	mvn(org.slf4j:slf4j-jdk14)

Requires:	java-11-openjdk-headless >= 1:11.0.0
Requires:	javapackages-filesystem
Requires:	apache-commons-codec
Requires:	apache-commons-lang
Requires:	jackson-core
Requires:	jackson-databind
Requires:	ovirt-engine-extensions-api
Requires:	slf4j
Requires:	slf4j-jdk14


%description
This package contains the oVirt Engine Local Users Management Extension
to manage users stored in PostgreSQL database.


%prep
%setup -c -q


%build
# Necessary to override the default for xmvn, which is JDK 8
export JAVA_HOME="/usr/lib/jvm/java-11-openjdk"

%mvn_build -j


%install
make %{make_common_opts} install
%mvn_install

%files -f .mfiles
%license LICENSE
%dir %{_javadir}/%{name}
%doc README.admin
%doc README.developer
%{_datadir}/%{name}/
%{_sysconfdir}/ovirt-engine/engine.conf.d/50-%{name}.conf
%{_bindir}/ovirt-aaa-jdbc-tool


%changelog
* Wed Jul 7 2021 lijunwei <lijunwei@kylinos.cn> 1.2.0-1
- Init Package

